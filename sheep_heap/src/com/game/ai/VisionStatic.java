//package com.game.ai;
//
//import java.awt.Point;
//import java.util.ArrayList;
//import java.util.List;
//
//import com.game.GMap;
//import com.game.object.Field;
//
//public class VisionStatic implements IVision {
//	private static int[][] DIRECTIONS;
//	private static final int[][] CARDINAL_DIRECTIONS = new int[][]{{-1,0},  {0,1},  {1,0},  {0, -1}};
//	private static final int[][] ALL_DIRECTIONS = new int[][] { { 0, -1 },
//		{ 1, -1 }, { 1, 0 }, { 1, 1 }, { 0, 1 }, { -1, 1 }, { -1, 0 },
//		{ -1, -1 } };
//	private int cols = 25;
//	private int rows = 25;
//	public char[][] matrix;
//	
//	Point center;
//	int radius = 7;
//	int angle = 180;
//	int counter = 0;
//	int rotation = 45;
//	//List<Field> fields;
//	List<Point> visited;	
//	@Override
//	public Field[] getVisionFields(Point pos, GMap map, int angle, int radius, int rotation) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//	@Override
//	public boolean isWaterInVision(Point pos, GMap map, int angle, int radius, int rotation) {
//		// TODO Auto-generated method stub
//		return false;
//	}
//	@Override
//	public boolean isFoodInVision(Point pos, GMap map, int angle, int radius, int rotation) {
//		// TODO Auto-generated method stub
//		return false;
//	}
//	@Override
//	public boolean isSheepInVision(Point pos, GMap map, int angle, int radius, int rotation) {
//		// TODO Auto-generated method stub
//		return false;
//	}
//	
//
//	public void vision_optimal() {
//		initMatrix(rows, cols);
//
//		center = new Point(cols / 2, rows / 2);
//		matrix[center.x][center.y] = 'c';
//
//		//markQuadrants();
//		//printMatrix();
//		int angle = 10;
//		while(angle < 360){
//			initMatrix(rows, cols);
//			System.out.println("For: "+angle+"°");
//			markCircualSector(angle, rotation);
//			printMatrix();
//			System.out.println(counter);
//			System.out.println("--------------------------------------------------------------");
//			counter = 0;
//			angle += 10;
//					
//		}
//	}
//	private List<Field> getVisionFields(final int angle, final int rotation) {
//		List<Field> fields = new ArrayList<Field>();
//		if (angle <= 180) {
//			// if angle is less than 180 degrees then apply algorithm
//			markCircularSectorFromTo(pos, radius, rotation, angle / 2, -angle / 2, 'X', fields);
//		} else {
//			// otherwise split circular sector in two. From 0° to left and from 0° to right 
//			markCircularSectorFromTo(pos, radius, rotation, angle / 2, 0, '1', fields);
//			markCircularSectorFromTo(pos, radius, rotation, 0, -angle / 2, '2', fields);
//		}
//
// 	public Field[] getVisionFieldsFromTo(final Point center,
//			final int radius, final int rotation, final int startAngle, final int endAngle, List<Field> fields) {
//		if(rotation % 90 == 0){
//			DIRECTIONS = CARDINAL_DIRECTIONS;
//		}
//		else{
//			DIRECTIONS = ALL_DIRECTIONS;
//		}
//		Point startPoint = getVectorOfAngle(radius, startAngle+rotation);
//		Point endPoint = getVectorOfAngle(radius, endAngle+rotation);
//		getVisionFieldsFromToRec(center, radius, startPoint, endPoint, fields);
//		return 	null;
//	}
//	
//	public void getVisionFieldsFromToRec(final Point point, final int radius,
//			final Point startPoint, final Point endPoint, List<Field> fields) {
//		List<Point> surroundings = getSurroundings(point, matrix);
//		for (Point s : surroundings) {
//			if (!visited.contains(s)
//					&& isInsideSector(s, center, startPoint, endPoint,
//							radius * radius)) {
//				counter++;
//				Field field = matrix[s.x][s.y];
//				visited.add(s);
//				markCircularSectorFromToRec(s, radius, startPoint,
//						endPoint, mark);
//			}
//		}
//	}
//
//
//	public static Point getVectorOfAngle(final int radius,
//			final int angleInDegrees) {
//		// Convert from degrees to radians via multiplication by PI/180
//		int x = (int) (radius * Math.cos(angleInDegrees * Math.PI / 180F));
//		int y = (int) (radius * Math.sin(angleInDegrees * Math.PI / 180F));
//
//		return new Point(x, y);
//	}
//
//	public static boolean areClockwise(Point v1, Point v2) {
//		if((v1.y < 0 && v1.x < 0) || (v1.y > 0 && v1.x > 0))
//			return -v1.x * v2.y + v1.y * v2.x > 0;
//		else
//			return -v1.x * v2.y + v1.y * v2.x >= 0;
//	}
//
//	public static boolean isWithinRadius(Point v, float radiusSquared) {
//		return v.x * v.x + v.y * v.y <= radiusSquared;
//	}
//
//	public static boolean isInsideSector(Point point, Point center,
//			Point sectorStart, Point sectorEnd, float radiusSquared) {
//		Point relPoint = new Point(point.x - center.x, point.y - center.y);
//
//		return !areClockwise(sectorStart, relPoint)
//				&& areClockwise(sectorEnd, relPoint)
//				&& isWithinRadius(relPoint, radiusSquared);
//	}
//
//	/**
//	 * Get list of all surroundings. 1 1 1 1 x 1 1 1 1
//	 * 
//	 * @param pos
//	 * @return
//	 */
//	public static List<Point> getSurroundings(Point pos, char matrix[][]) {
//		List<Point> surroundings = new ArrayList<>();
//		for (int[] direction : DIRECTIONS) {
//			int cx = pos.x + direction[0];
//			int cy = pos.y + direction[1];
//			if (cy >= 0 && cy < matrix.length)
//				if (cx >= 0 && cx < matrix[cy].length) {
//					surroundings.add(new Point(cx, cy));
//				}
//		}
//		return surroundings;
//	}
//
//
//}
