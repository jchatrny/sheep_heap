package com.game.ai;

import java.awt.Point;
import java.util.List;

public interface IVision {
	public  List<Point> getVisionFieldsPos(Point pos, int rotation, int visionAngle, int radius);
	//public boolean isWaterInVision(Point pos, int rotation, int visionAngle, int radius);
	//public boolean isFoodInVision(Point pos, int rotation, int visionAngle, int radius);
	//public boolean isSheepInVision(Point pos, int rotation, int visionAngle, int radius);
}
