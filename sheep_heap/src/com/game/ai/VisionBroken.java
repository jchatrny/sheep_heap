package com.game.ai;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import com.game.GMap;
import com.game.object.Field;

public class VisionBroken {
	private static int[][] DIRECTIONS;
	private static final int[][] CARDINAL_DIRECTIONS = new int[][]{{-1,0},  {0,1},  {1,0},  {0, -1}};
	private static final int[][] ALL_DIRECTIONS = new int[][] { { 0, -1 },
		{ 1, -1 }, { 1, 0 }, { 1, 1 }, { 0, 1 }, { -1, 1 }, { -1, 0 },
		{ -1, -1 } };
		
	private GMap map;
	
	private Point center;
	private int radius;
	//private int angle;
	//private int counter = 0;
	//private int rotation;
	private List<Point> visited;
	private List<Field> fields;
	
	public VisionBroken(GMap map,final int radius){
		this.map = map;
		this.radius = radius;
		visited = new ArrayList<Point>();
		fields = new ArrayList<Field>();
	}
	
	public List<Field> getVisionFields(Point pos, int angle, int radius, int rotation) {
		fields.clear();
		visited.clear();
		this.center = pos;
		this.radius = radius;
		getVisionFields(angle, rotation);

		//this.angle = angle;
		//this.rotation = rotation;
		return fields;
	}

	public boolean isWaterInVision(Point pos, int angle, int radius, int rotation) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean isFoodInVision(Point pos, int angle, int radius, int rotation) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean isSheepInVision(Point pos, int angle, int radius, int rotation) {
		// TODO Auto-generated method stub
		return false;
	}
	
	private void getVisionFields(final int angle, final int rotation) {
		if (angle <= 180) {
			// if angle is less than 180 degrees then apply algorithm
			getVisionFieldsFromTo(center, radius, rotation, angle / 2, -angle / 2);
		} else {
			// otherwise split circular sector in two. From 0° to left and from 0° to right 
			getVisionFieldsFromTo(center, radius, rotation, angle / 2, 0);
			getVisionFieldsFromTo(center, radius, rotation, 0, -angle / 2);
		}
	}

 	public void getVisionFieldsFromTo(final Point center,
			final int radius, final int rotation, final int startAngle, final int endAngle) {
		if(rotation % 90 == 0){
			DIRECTIONS = CARDINAL_DIRECTIONS;
		}
		else{
			DIRECTIONS = ALL_DIRECTIONS;
		}
		Point startPoint = getVectorOfAngle(radius, startAngle+rotation);
		Point endPoint = getVectorOfAngle(radius, endAngle+rotation);
		getVisionFieldsFromToRec(center, radius, startPoint, endPoint);
	}
 	
	
	public void getVisionFieldsFromToRec(final Point point, final int radius,
			final Point startPoint, final Point endPoint) {
		List<Field> surroundings = getSurroundings(point, map.getFields());
		for (Field f : surroundings) {
			if (!visited.contains(f)
					&& isInsideSector(f.getPosition(), startPoint, endPoint,
							radius * radius)) {
				//counter++;
				visited.add(f.getPosition());
				fields.add(f);
				getVisionFieldsFromToRec(f.getPosition(), radius, startPoint,
						endPoint);
			}
		}
	}
 	
 	
	private Point getVectorOfAngle(final int radius,
			final int angleInDegrees) {
		// Convert from degrees to radians via multiplication by PI/180
		int x = (int) (radius * Math.cos(angleInDegrees * Math.PI / 180F));
		int y = (int) (radius * Math.sin(angleInDegrees * Math.PI / 180F));

		return new Point(x, y);
	}

	private boolean areClockwise(Point v1, Point v2) {
		if((v1.y < 0 && v1.x < 0) || (v1.y > 0 && v1.x > 0))
			return -v1.x * v2.y + v1.y * v2.x > 0;
		else
			return -v1.x * v2.y + v1.y * v2.x >= 0;
	}

	private boolean isWithinRadius(Point v, float radiusSquared) {
		return v.x * v.x + v.y * v.y <= radiusSquared;
	}

	private boolean isInsideSector(Point point,
			Point sectorStart, Point sectorEnd, float radiusSquared) {
		Point relPoint = new Point(point.x - center.x, point.y - center.y);

		return !areClockwise(sectorStart, relPoint)
				&& areClockwise(sectorEnd, relPoint)
				&& isWithinRadius(relPoint, radiusSquared);
	}
	
	/**
	 * Get list of all surroundings. 1 1 1 1 x 1 1 1 1
	 * 
	 * @param pos
	 * @return
	 */
	public static List<Field> getSurroundings(Point pos, Field matrix[][]) {
		List<Field> surroundings = new ArrayList<Field>();
		for (int[] direction : DIRECTIONS) {
			int cx = pos.x + direction[0];
			int cy = pos.y + direction[1];
			if (cy >= 0 && cy < matrix.length)
				if (cx >= 0 && cx < matrix[cy].length) {
					surroundings.add(matrix[cx][cy]);
				}
		}
		return surroundings;
	}

}
