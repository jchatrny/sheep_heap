package com.game.resource;

import static com.game.Global.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

/**
 * Mediates sprites to game. If font doesn't exist, try load it from resources and save for further usage.
 * @author jchatrny
 *
 */
public class TextureFactory {
	public static Map<String, Texture> textures = new HashMap<String, Texture>();
	/**
	 * Initialize factory
	 */
	public TextureFactory() {
		textures = new HashMap<String, Texture>();
	}

	/**
	 * Returns sprite
	 * @param name
	 * @return
	 */
	public static Texture get(String name) {

		Texture sprite;

		sprite = textures.get(name);

		if (null == sprite) {
			sprite = loadTexture(name);
			textures.put(name, sprite);
			if(DEBUG_RESOURCE)
				System.out.println("New sprite loaded");
		}

		return sprite;
	}

	/**
	 * Loads PNG texture from resource path. 128x128
	 * 
	 * @param key
	 *            name of file
	 * @return
	 */
	public static Texture loadTexture(String key) {
		try {
			return TextureLoader.getTexture("png", new FileInputStream(
					new File(RES_PATH + key + ".png")));
		} catch (IOException e) {
			System.err.println("Loading sprite file:\""+RES_PATH + key + ".png"+"\" failed.");
			e.printStackTrace();
		}
		return null;
	}
}
