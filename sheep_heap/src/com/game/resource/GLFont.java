package com.game.resource;

import java.util.HashMap;
import java.util.Map;

import org.newdawn.slick.TrueTypeFont;

/**
 * Represents group of fonts from same family with different sizes.
 * @author jchatrny
 *
 */
public class GLFont {
	private Map<Integer,TrueTypeFont> specificFont;
	/**
	 * Creates empty font group
	 */
	public GLFont(){
		specificFont = new HashMap<Integer,TrueTypeFont>();
	}
	/**
	 * Puts new font.
	 * @param key
	 * @param ttf
	 */
	public void put(int key, TrueTypeFont ttf){
		specificFont.put(key, ttf);
	}
	/**
	 * Gets font with specific size. 
	 * @param key
	 * @return
	 */
	public TrueTypeFont get(int key){
		return specificFont.get(key);
	}
		
}
