package com.game.resource;

import java.awt.Font;
import java.util.HashMap;
import java.util.Map;

import org.newdawn.slick.TrueTypeFont;
import static com.game.Global.*;

/**
 * Mediates fonts to game. If font doesn't exist, convert it from java.awt font
 * and save for further usage.
 * 
 * @author jchatrny
 *
 */
public class FontFactory {
	public static Map<String, GLFont> fonts = new HashMap<String, GLFont>();
	public static int capacity;

	/**
	 * Create empty factory
	 */
	public FontFactory() {
		fonts = new HashMap<String, GLFont>();
	}

	/**
	 * Get needed font, if font doesn't exist convert him and save.
	 * 
	 * @param name
	 * @param style
	 * @param size
	 * @return
	 */
	public static TrueTypeFont getFont(String name, int style, int size) {

		TrueTypeFont ttf;
		GLFont font;

		font = fonts.get(name);
		if (null == font) { // font not found
			if(DEBUG_RESOURCE)
				System.out.println("New TrueTypeFont created " + (++capacity));
			ttf = convertFontFromAwt(name, Font.PLAIN, size);
			// save
			font = new GLFont();
			font.put(size, ttf);
			fonts.put(name, font);

			return ttf;
		} else {
			ttf = font.get(size);
			if (null == ttf) {
				if(DEBUG_RESOURCE)
					System.out.println("New TrueTypeFont created " + (++capacity));
				ttf = convertFontFromAwt(name, Font.PLAIN, size);
				font.put(size, ttf);
			}
			return ttf;
		}
	}

	/**
	 * Convert font from java.atw to TrueTypeFont
	 * 
	 * @param name
	 * @param style
	 * @param size
	 * @return
	 */
	private static TrueTypeFont convertFontFromAwt(String name, int style, int size) {
		Font awtFont = new Font(name, style, size);
		return new TrueTypeFont(awtFont, true);
	}
}
