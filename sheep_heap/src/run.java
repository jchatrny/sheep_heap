import com.engine.Engine;
import com.game.Game;
import com.game.Global;


public class run {

	public static void main(String[] args) {
		Global.loadConfig();
		Engine.init(800, 600);
		Game game = new Game();
		Engine.initGame(game);
		Engine.run();
	}

}
